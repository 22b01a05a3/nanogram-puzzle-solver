def input() -> list[list[list]]:
    row_clues = [[1, 2], [2], [2], [2], [4]]
    col_clues = [[1, 3], [3], [1], [2, 1], [2]]
    return [row_clues, col_clues]
