def solve_nonogram(input):
    rows = input[0]
    cols = input[1]

    grid = [['.'] * cols for _ in range(rows)]

    def backtrack(row, col):
        if row == rows:
            return True

        if col == cols:
            return backtrack(row + 1, 0)
l
        if is_valid_cell(row, col, grid, row_clues, col_clues):
            grid[row][col] = '#'
            if backtrack(row, col + 1):
                return True
            grid[row][col] = '.'

        if is_valid_cell(row, col, grid, row_clues, col_clues):
            grid[row][col] = '.'
            if backtrack(row, col + 1):
                return True
            grid[row][col] = '.'

        return False

    if backtrack(0, 0):
        return grid  
    else:
        return None  


def is_valid_cell(row, col, grid, row_clues, col_clues):
     return is_valid_row(row, grid, row_clues) and is_valid_column(col, grid, col_clues)


def is_valid_row(row, grid, row_clues):
    cclues = row_clues[row]
    row_cells = grid[row]
    clue_index = 0
    clue_count = 0

    for cell in row_cells:
        if cell == '#':
            clue_count += 1
            if clue_count > len(clues):
                return False
        elif cell == '.':
            if clue_count > 0 and clue_count != clues[clue_index]:
                return False
            if clue_count > 0:
                clue_index += 1
            clue_count = 0

    if clue_count > 0 and clue_count != clues[clue_index]:
        return False

    return clue_index == len(clues) - 1


def is_valid_column(col, grid, col_clues):
    clues = col_clues[col]
    col_cells = [grid[row][col] for row in range(len(grid))]
    clue_index = 0
    clue_count = 0

    for cell in col_cells:
        if cell == '#':
            clue_count += 1
            if clue_count > len(clues):
                return False
        elif cell == '.':
            if clue_count > 0 and clue_count != clues[clue_index]:
                return False
            if clue_count > 0:
                clue_index += 1
            clue_count = 0

    if clue_count > 0 and clue_count != clues[clue_index]:
        return False

    return clue_index == len(clues) - 1

