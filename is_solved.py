def is_solved(nonogram: list) -> bool:
    for i in nonogram:
        if i == 0:
           return False
    return True
